NetworkDirect-sys
=================

This crate provides Rust bindings for Microsoft's NetworkDirect SPI, version 2.0. It does this by downloading the official
NetworkDirect package from [NuGet](https://www.nuget.org/packages/NetworkDirect) and generating bindings to it using `bindgen`.

>The NetworkDirect architecture provides application developers with a networking interface that enables zero-copy data transfers between applications, kernel-bypass I/O generation and completion processing, and one-sided data transfer operations. The NetworkDirect service provider interface (SPI) defines the interface that NetworkDirect providers implement to expose their hardware capabilities to applications.

Documentation
-------------

Documentation for NetworkDirect APIs can be found [in Microsoft's repository](https://github.com/microsoft/NetworkDirect/blob/master/docs/NetworkDirectSPI.md).

License
-------

MIT