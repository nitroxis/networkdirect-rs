fn main() {
	let factories = networkdirect::all_providers().unwrap();
	for mut factory in factories {
		println!("ID: {:?}", factory.id());
		println!("Path: {}", factory.path().unwrap());

		let provider = factory.get_provider().unwrap();
		let addrs = provider.query_address_list().unwrap();

		println!("Addresses:");
		for addr in addrs {
			println!(" - {addr:?}");
		}

		println!();
	}
}
