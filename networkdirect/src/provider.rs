use std::{
	net::{IpAddr, SocketAddr},
	ptr,
};

use networkdirect_sys::*;
use windows::{core::Result, Win32::Networking::WinSock::SOCKET_ADDRESS_LIST};

use crate::{
	util::{from_winsock, to_winsock},
	Adapter,
};

pub struct Provider {
	inner: *mut IND2Provider,
}

impl Provider {
	pub fn vtable(&self) -> &IND2ProviderVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2Provider instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2Provider) -> Provider {
		Self { inner: ptr }
	}

	pub fn query_address_list(&self) -> Result<Vec<IpAddr>> {
		unsafe {
			let mut size = 0;
			let ptr = ptr::null_mut();
			let res = self.vtable().QueryAddressList.unwrap()(self.inner, ptr, &mut size);
			if res != ND_BUFFER_OVERFLOW {
				res.ok()?;
			}

			let mut data = vec![0u8; size as usize];
			self.vtable().QueryAddressList.unwrap()(self.inner, data.as_mut_ptr() as _, &mut size).ok()?;

			let list: &SOCKET_ADDRESS_LIST = &*(data.as_ptr() as *const _);
			let mut result = Vec::with_capacity(list.iAddressCount as _);

			for i in 0..(list.iAddressCount as usize) {
				let addr = &*list.Address.get_unchecked(i).lpSockaddr;
				if let Some(addr) = from_winsock(addr) {
					result.push(addr.ip());
				}
			}

			Ok(result)
		}
	}

	pub fn resolve_address(&self, addr: SocketAddr) -> Result<u64> {
		unsafe {
			let (addr, addr_len) = to_winsock(addr);
			let mut adapter_id = 0;
			self.vtable().ResolveAddress.unwrap()(self.inner, &addr as *const _ as *const _, addr_len, &mut adapter_id).ok()?;
			Ok(adapter_id)
		}
	}

	pub fn open_adapter(&self, adapter_id: u64) -> Result<Adapter> {
		unsafe {
			let mut adapter = ptr::null_mut();
			self.vtable().OpenAdapter.unwrap()(self.inner, &IID_IND2Adapter, adapter_id, &mut adapter).ok()?;

			Ok(Adapter::from_ptr(adapter as *mut _))
		}
	}
}

unsafe impl Send for Provider {}

impl Clone for Provider {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Provider->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for Provider {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Provider->Release()");
		}
	}
}
