use bitflags::bitflags;
use networkdirect_sys::*;
use windows::core::Result;

use crate::{RemoteToken, RequestContext};

bitflags! {
	pub struct SendFlags: u32 {
		const SILENT_SUCCESS = ND_OP_FLAG_SILENT_SUCCESS;
		const READ_FENCE = ND_OP_FLAG_READ_FENCE;
		const SEND_AND_SOLICIT_EVENT = ND_OP_FLAG_SEND_AND_SOLICIT_EVENT;
		const INLINE = ND_OP_FLAG_INLINE;
	}

	pub struct BindFlags: u32 {
		const SILENT_SUCCESS = ND_OP_FLAG_SILENT_SUCCESS;
		const READ_FENCE = ND_OP_FLAG_READ_FENCE;
		const ALLOW_READ = ND_OP_FLAG_ALLOW_READ;
		const ALLOW_WRITE = ND_OP_FLAG_ALLOW_WRITE;
	}

	pub struct InvalidateFlags: u32 {
		const SILENT_SUCCESS = ND_OP_FLAG_SILENT_SUCCESS;
		const READ_FENCE = ND_OP_FLAG_READ_FENCE;
	}

	pub struct ReadFlags: u32 {
		const SILENT_SUCCESS = ND_OP_FLAG_SILENT_SUCCESS;
		const READ_FENCE = ND_OP_FLAG_READ_FENCE;
	}

	pub struct WriteFlags: u32 {
		const SILENT_SUCCESS = ND_OP_FLAG_SILENT_SUCCESS;
		const READ_FENCE = ND_OP_FLAG_READ_FENCE;
		const INLINE = ND_OP_FLAG_INLINE;
	}
}

pub struct QueuePair {
	pub(crate) inner: *mut IND2QueuePair,
}

impl AsRef<IND2QueuePair> for QueuePair {
	fn as_ref(&self) -> &IND2QueuePair {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2QueuePair> for QueuePair {
	fn as_mut(&mut self) -> &mut IND2QueuePair {
		unsafe { &mut *self.inner }
	}
}

impl QueuePair {
	pub fn vtable(&self) -> &IND2QueuePairVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2QueuePair instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2QueuePair) -> QueuePair {
		Self { inner: ptr }
	}

	pub fn flush(&mut self) -> Result<()> {
		unsafe { self.vtable().Flush.unwrap()(self.inner).ok() }
	}

	pub fn send(&mut self, request_context: RequestContext, sge: &[ND2_SGE], flags: SendFlags) -> Result<()> {
		unsafe {
			self.vtable().Send.unwrap()(
				self.inner,
				request_context.as_ptr(),
				sge.as_ptr(),
				sge.len() as u32,
				flags.bits(),
			)
			.ok()
		}
	}

	pub fn receive(&mut self, request_context: RequestContext, sge: &[ND2_SGE]) -> Result<()> {
		unsafe { self.vtable().Receive.unwrap()(self.inner, request_context.as_ptr(), sge.as_ptr(), sge.len() as u32).ok() }
	}

	pub fn bind<T>(
		&mut self,
		request_context: RequestContext,
		memory_region: &impl AsRef<IND2MemoryRegion>,
		memory_window: &impl AsRef<IND2MemoryWindow>,
		buffer: &[T],
		flags: BindFlags,
	) -> Result<()> {
		unsafe {
			self.vtable().Bind.unwrap()(
				self.inner,
				request_context.as_ptr(),
				memory_region.as_ref() as *const _ as *mut _,
				memory_window.as_ref() as *const _ as *mut _,
				buffer.as_ptr() as _,
				buffer.len() as _,
				flags.bits(),
			)
			.ok()
		}
	}

	pub fn invalidate<T>(
		&mut self,
		request_context: RequestContext,
		memory_window: &impl AsRef<IND2MemoryWindow>,
		flags: InvalidateFlags,
	) -> Result<()> {
		unsafe {
			self.vtable().Invalidate.unwrap()(
				self.inner,
				request_context.as_ptr(),
				memory_window.as_ref() as *const _ as *mut _,
				flags.bits(),
			)
			.ok()
		}
	}

	pub fn read(
		&mut self,
		request_context: RequestContext,
		sge: &[ND2_SGE],
		remote_address: u64,
		remote_token: RemoteToken,
		flags: ReadFlags,
	) -> Result<()> {
		unsafe {
			self.vtable().Read.unwrap()(
				self.inner,
				request_context.as_ptr(),
				sge.as_ptr(),
				sge.len() as u32,
				remote_address,
				remote_token.0,
				flags.bits(),
			)
			.ok()
		}
	}

	pub fn write(
		&mut self,
		request_context: RequestContext,
		sge: &[ND2_SGE],
		remote_address: u64,
		remote_token: RemoteToken,
		flags: WriteFlags,
	) -> Result<()> {
		unsafe {
			self.vtable().Write.unwrap()(
				self.inner,
				request_context.as_ptr(),
				sge.as_ptr(),
				sge.len() as u32,
				remote_address,
				remote_token.0,
				flags.bits(),
			)
			.ok()
		}
	}
}

unsafe impl Send for QueuePair {}

impl Clone for QueuePair {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2QueuePair->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for QueuePair {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2QueuePair->Release()");
		}
	}
}
