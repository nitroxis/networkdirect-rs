use std::borrow::BorrowMut;

use networkdirect_sys::*;
use windows::{core::Result, Win32::System::IO::OVERLAPPED};

use crate::ND2Overlapped;

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum NotifyType {
	Errors,
	Any,
	Solicited,
}

impl NotifyType {
	fn to_u32(self) -> u32 {
		match self {
			NotifyType::Errors => ND_CQ_NOTIFY_ERRORS,
			NotifyType::Any => ND_CQ_NOTIFY_ANY,
			NotifyType::Solicited => ND_CQ_NOTIFY_SOLICITED,
		}
	}
}

pub struct CompletionQueue {
	pub(crate) inner: *mut IND2CompletionQueue,
}

impl AsRef<IND2CompletionQueue> for CompletionQueue {
	fn as_ref(&self) -> &IND2CompletionQueue {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2CompletionQueue> for CompletionQueue {
	fn as_mut(&mut self) -> &mut IND2CompletionQueue {
		unsafe { &mut *self.inner }
	}
}

impl CompletionQueue {
	pub fn vtable(&self) -> &IND2CompletionQueueVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2CompletionQueue instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2CompletionQueue) -> CompletionQueue {
		Self { inner: ptr }
	}

	pub fn notify_affinity(&self) -> Result<(u16, KAFFINITY)> {
		unsafe {
			let mut group = 0;
			let mut affinity = 0;
			self.vtable().GetNotifyAffinity.unwrap()(self.inner, &mut group, &mut affinity).ok()?;
			Ok((group, affinity))
		}
	}

	pub fn resize(&mut self, queue_depth: u32) -> Result<()> {
		unsafe { self.vtable().Resize.unwrap()(self.inner, queue_depth).ok() }
	}

	pub fn notify(&mut self, type_: NotifyType, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<()> {
		unsafe {
			let res = self.vtable().Notify.unwrap()(self.inner, type_.to_u32(), overlapped.borrow_mut());

			if res == ND_PENDING {
				self.get_overlapped_result(overlapped.borrow_mut(), true)
			} else {
				res.ok()
			}
		}
	}

	pub fn results(&mut self, results: &mut [ND2_RESULT]) -> u32 {
		unsafe { self.vtable().GetResults.unwrap()(self.inner, results.as_mut_ptr(), results.len() as u32) }
	}

	pub fn poll(&mut self, type_: NotifyType, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<ND2_RESULT> {
		let mut temp = [ND2_RESULT::default(); 1];
		while self.results(&mut temp) == 0 {
			self.notify(type_, overlapped.borrow_mut())?;
		}

		let [res] = temp;
		Ok(res)
	}
}

impl ND2Overlapped for CompletionQueue {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped {
		unsafe { &mut *(self.inner as *mut IND2Overlapped) }
	}
}

unsafe impl Send for CompletionQueue {}

impl Clone for CompletionQueue {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2CompletionQueue->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for CompletionQueue {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2CompletionQueue->Release()");
		}
	}
}
