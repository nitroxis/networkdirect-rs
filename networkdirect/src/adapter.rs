use std::net::IpAddr;
use std::{fs::File, mem, os::windows::io::*, ptr};

use networkdirect_sys::*;
use windows::core::Result;
use windows::Win32::Foundation::HANDLE;
use windows::Win32::Networking::WinSock::SOCKET_ADDRESS_LIST;

use crate::{util::from_winsock, CompletionQueue, QueuePair, UnregisteredMemoryRegion};
use crate::{Connector, Listener, SharedReceiveQueue};

pub struct Adapter {
	pub(crate) inner: *mut IND2Adapter,
}

impl AsRef<IND2Adapter> for Adapter {
	fn as_ref(&self) -> &IND2Adapter {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2Adapter> for Adapter {
	fn as_mut(&mut self) -> &mut IND2Adapter {
		unsafe { &mut *self.inner }
	}
}

impl Adapter {
	fn vtable(&self) -> &IND2AdapterVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2Adapter instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2Adapter) -> Adapter {
		Self { inner: ptr }
	}

	pub fn create_overlapped_file(&mut self) -> Result<std::fs::File> {
		let mut file: HANDLE = HANDLE::default();
		unsafe {
			self.vtable().CreateOverlappedFile.unwrap()(self.inner, &mut file).ok()?;
			Ok(File::from_raw_handle(file.0 as _))
		}
	}

	pub fn query(&self) -> Result<ND2_ADAPTER_INFO> {
		let mut info = ND2_ADAPTER_INFO {
			InfoVersion: ND_VERSION_2,
			..Default::default()
		};
		let mut info_size = mem::size_of::<ND2_ADAPTER_INFO>() as u32;

		unsafe { self.vtable().Query.unwrap()(self.inner, &mut info, &mut info_size).ok()? }

		Ok(info)
	}

	pub fn query_address_list(&self) -> Result<Vec<IpAddr>> {
		unsafe {
			let mut size = 0;
			let ptr = ptr::null_mut();
			let res = self.vtable().QueryAddressList.unwrap()(self.inner, ptr, &mut size);
			if res != ND_BUFFER_OVERFLOW {
				res.ok()?;
			}

			let mut data = vec![0u8; size as usize];
			self.vtable().QueryAddressList.unwrap()(self.inner, data.as_mut_ptr() as _, &mut size).ok()?;

			let list: &SOCKET_ADDRESS_LIST = &*(data.as_ptr() as *const _);
			let mut result = Vec::with_capacity(list.iAddressCount as _);

			for i in 0..(list.iAddressCount as usize) {
				let addr = &*list.Address.get_unchecked(i).lpSockaddr;
				if let Some(addr) = from_winsock(addr) {
					result.push(addr.ip());
				}
			}

			Ok(result)
		}
	}

	pub fn create_memory_region(&mut self, file: &mut std::fs::File) -> Result<UnregisteredMemoryRegion> {
		unsafe {
			let mut mr = ptr::null_mut();
			self.vtable().CreateMemoryRegion.unwrap()(
				self.inner,
				&IID_IND2MemoryRegion,
				HANDLE(file.as_raw_handle() as _),
				&mut mr,
			)
			.ok()?;
			Ok(UnregisteredMemoryRegion::from_ptr(mr as *mut _))
		}
	}

	pub fn create_shared_receive_queue(
		&mut self,
		overlapped_file: &impl AsRawHandle,
		queue_depth: u32,
		max_request_sge: u32,
		notify_threshold: u32,
		group: u16,
		affinity: KAFFINITY,
	) -> Result<SharedReceiveQueue> {
		unsafe {
			let fd = HANDLE(overlapped_file.as_raw_handle() as _);
			let mut srq = ptr::null_mut();
			self.vtable().CreateSharedReceiveQueue.unwrap()(
				self.inner,
				&IID_IND2SharedReceiveQueue,
				fd,
				queue_depth,
				max_request_sge,
				notify_threshold,
				group,
				affinity,
				&mut srq,
			)
			.ok()?;

			Ok(SharedReceiveQueue::from_ptr(srq as *mut _))
		}
	}

	pub fn create_completion_queue(
		&mut self,
		overlapped_file: &impl AsRawHandle,
		queue_depth: u32,
		group: u16,
		affinity: KAFFINITY,
	) -> Result<CompletionQueue> {
		unsafe {
			let fd = HANDLE(overlapped_file.as_raw_handle() as _);
			let mut cq = ptr::null_mut();
			self.vtable().CreateCompletionQueue.unwrap()(
				self.inner,
				&IID_IND2CompletionQueue,
				fd,
				queue_depth,
				group,
				affinity,
				&mut cq,
			)
			.ok()?;

			Ok(CompletionQueue::from_ptr(cq as *mut _))
		}
	}

	#[allow(clippy::too_many_arguments)]
	pub fn create_queue_pair(
		&mut self,
		receive_completion_queue: &impl AsRef<IND2CompletionQueue>,
		initiator_completion_queue: &impl AsRef<IND2CompletionQueue>,
		receive_queue_depth: u32,
		initiator_queue_depth: u32,
		max_receive_request_sge: u32,
		max_initiator_request_sge: u32,
		inline_data_size: u32,
	) -> Result<QueuePair> {
		unsafe {
			let mut qp = ptr::null_mut();
			self.vtable().CreateQueuePair.unwrap()(
				self.inner,
				&IID_IND2QueuePair,
				receive_completion_queue.as_ref() as *const _ as *mut _,
				initiator_completion_queue.as_ref() as *const _ as *mut _,
				ptr::null_mut(),
				receive_queue_depth,
				initiator_queue_depth,
				max_receive_request_sge,
				max_initiator_request_sge,
				inline_data_size,
				&mut qp,
			)
			.ok()?;

			Ok(QueuePair::from_ptr(qp as *mut _))
		}
	}

	#[allow(clippy::too_many_arguments)]
	pub fn create_queue_pair_with_srq(
		&mut self,
		receive_completion_queue: &impl AsRef<IND2CompletionQueue>,
		initiator_completion_queue: &impl AsRef<IND2CompletionQueue>,
		shared_receive_queue: &impl AsRef<IND2SharedReceiveQueue>,
		max_receive_request_sge: u32,
		max_initiator_request_sge: u32,
		inline_data_size: u32,
	) -> Result<QueuePair> {
		unsafe {
			let mut qp = ptr::null_mut();
			self.vtable().CreateQueuePairWithSrq.unwrap()(
				self.inner,
				&IID_IND2QueuePair,
				receive_completion_queue.as_ref() as *const _ as *mut _,
				initiator_completion_queue.as_ref() as *const _ as *mut _,
				shared_receive_queue.as_ref() as *const _ as *mut _,
				ptr::null_mut(),
				max_receive_request_sge,
				max_initiator_request_sge,
				inline_data_size,
				&mut qp,
			)
			.ok()?;

			Ok(QueuePair::from_ptr(qp as *mut _))
		}
	}

	pub fn create_connector(&mut self, overlapped_file: &impl AsRawHandle) -> Result<Connector> {
		unsafe {
			let fd = HANDLE(overlapped_file.as_raw_handle() as _);
			let mut conn = ptr::null_mut();
			self.vtable().CreateConnector.unwrap()(self.inner, &IID_IND2Connector, fd, &mut conn).ok()?;

			Ok(Connector::from_ptr(conn as *mut _))
		}
	}

	pub fn create_listener(&mut self, overlapped_file: &impl AsRawHandle) -> Result<Listener> {
		unsafe {
			let fd = HANDLE(overlapped_file.as_raw_handle() as _);
			let mut listener = ptr::null_mut();
			self.vtable().CreateListener.unwrap()(self.inner, &IID_IND2Listener, fd, &mut listener).ok()?;

			Ok(Listener::from_ptr(listener as *mut _))
		}
	}
}

impl Clone for Adapter {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Adapter->AddRef()");
		}

		Self { inner: self.inner }
	}
}

unsafe impl Send for Adapter {}

impl Drop for Adapter {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Adapter->Release()");
		}
	}
}
