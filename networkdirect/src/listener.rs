use std::{borrow::BorrowMut, net::SocketAddr};

use networkdirect_sys::*;
use windows::{core::Result, Win32::System::IO::OVERLAPPED};

use crate::{
	util::{from_winsock_fn, to_winsock},
	Connector, ND2Overlapped,
};

pub struct Listener {
	pub(crate) inner: *mut IND2Listener,
}

impl AsRef<IND2Listener> for Listener {
	fn as_ref(&self) -> &IND2Listener {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2Listener> for Listener {
	fn as_mut(&mut self) -> &mut IND2Listener {
		unsafe { &mut *self.inner }
	}
}

impl Listener {
	pub fn vtable(&self) -> &IND2ListenerVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2Listener instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2Listener) -> Listener {
		Self { inner: ptr }
	}

	pub fn bind(&mut self, address: impl Into<SocketAddr>) -> Result<()> {
		let (addr, addr_len) = to_winsock(address.into());

		unsafe { self.vtable().Bind.unwrap()(self.inner, &addr as *const _ as *const _, addr_len).ok() }
	}

	pub fn listen(&mut self, backlog: u32) -> Result<()> {
		unsafe { self.vtable().Listen.unwrap()(self.inner, backlog).ok() }
	}

	pub fn get_local_address(&self) -> Result<SocketAddr> {
		unsafe { from_winsock_fn(self.inner, self.vtable().GetLocalAddress.unwrap()) }
	}

	pub fn get_connection_request(
		&mut self,
		connector: &mut Connector,
		mut overlapped: impl BorrowMut<OVERLAPPED>,
	) -> Result<()> {
		unsafe {
			let res =
				self.vtable().GetConnectionRequest.unwrap()(self.inner, connector.inner as *mut _, overlapped.borrow_mut());

			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}
}

impl ND2Overlapped for Listener {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped {
		unsafe { &mut *(self.inner as *mut IND2Overlapped) }
	}
}

unsafe impl Send for Listener {}

impl Clone for Listener {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Listener->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for Listener {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Listener->Release()");
		}
	}
}
