use networkdirect_sys::*;

use crate::RemoteToken;

pub struct MemoryWindow {
	pub(crate) inner: *mut IND2MemoryWindow,
}

impl AsRef<IND2MemoryWindow> for MemoryWindow {
	fn as_ref(&self) -> &IND2MemoryWindow {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2MemoryWindow> for MemoryWindow {
	fn as_mut(&mut self) -> &mut IND2MemoryWindow {
		unsafe { &mut *self.inner }
	}
}

impl MemoryWindow {
	pub fn vtable(&self) -> &IND2MemoryWindowVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2MemoryWindow instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2MemoryWindow) -> MemoryWindow {
		Self { inner: ptr }
	}

	pub fn remote_token(&self) -> RemoteToken {
		RemoteToken(unsafe { self.vtable().GetRemoteToken.unwrap()(self.inner) })
	}
}

unsafe impl Send for MemoryWindow {}

impl Clone for MemoryWindow {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2MemoryWindow->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for MemoryWindow {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2MemoryWindow->Release()");
		}
	}
}
