use std::{borrow::BorrowMut, net::SocketAddr, ptr};

use networkdirect_sys::*;
use windows::{core::Result, Win32::System::IO::OVERLAPPED};

use crate::{
	util::{from_winsock_fn, to_winsock},
	ND2Overlapped, QueuePair,
};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ReadLimits {
	pub inbound_read_limit: u32,
	pub outbound_read_limit: u32,
}

pub struct Connector {
	pub(crate) inner: *mut IND2Connector,
}

impl AsRef<IND2Connector> for Connector {
	fn as_ref(&self) -> &IND2Connector {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2Connector> for Connector {
	fn as_mut(&mut self) -> &mut IND2Connector {
		unsafe { &mut *self.inner }
	}
}

impl Connector {
	pub fn vtable(&self) -> &IND2ConnectorVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2Connector instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2Connector) -> Connector {
		Self { inner: ptr }
	}

	pub fn bind(&mut self, address: impl Into<SocketAddr>) -> Result<()> {
		let (addr, addr_len) = to_winsock(address.into());

		unsafe { self.vtable().Bind.unwrap()(self.inner, &addr as *const _ as *const _, addr_len).ok() }
	}

	pub fn connect(
		&mut self,
		queue_pair: &QueuePair,
		dest_address: impl Into<SocketAddr>,
		limits: ReadLimits,
		private_data: Option<&[u8]>,
		mut overlapped: impl BorrowMut<OVERLAPPED>,
	) -> Result<()> {
		let (addr, addr_len) = to_winsock(dest_address.into());
		unsafe {
			let (data_ptr, data_len) = private_data
				.map(|s| (s.as_ptr(), s.len()))
				.unwrap_or_else(|| (ptr::null(), 0));

			let res = self.vtable().Connect.unwrap()(
				self.inner,
				queue_pair.inner as *mut _,
				&addr as *const _ as *const _,
				addr_len,
				limits.inbound_read_limit,
				limits.outbound_read_limit,
				data_ptr as *const _,
				data_len as u32,
				overlapped.borrow_mut(),
			);

			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}

	pub fn complete_connect(&mut self, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<()> {
		unsafe {
			let res = self.vtable().CompleteConnect.unwrap()(self.inner, overlapped.borrow_mut());
			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}

	pub fn accept(
		&mut self,
		queue_pair: &QueuePair,
		limits: ReadLimits,
		private_data: Option<&[u8]>,
		mut overlapped: impl BorrowMut<OVERLAPPED>,
	) -> Result<()> {
		unsafe {
			let (data_ptr, data_len) = private_data
				.map(|s| (s.as_ptr(), s.len()))
				.unwrap_or_else(|| (ptr::null(), 0));

			let res = self.vtable().Accept.unwrap()(
				self.inner,
				queue_pair.inner as *mut _,
				limits.inbound_read_limit,
				limits.outbound_read_limit,
				data_ptr as *const _,
				data_len as u32,
				overlapped.borrow_mut(),
			);
			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}

	pub fn reject(&mut self, private_data: Option<&[u8]>) -> Result<()> {
		unsafe {
			let (data_ptr, data_len) = private_data
				.map(|s| (s.as_ptr(), s.len()))
				.unwrap_or_else(|| (ptr::null(), 0));

			self.vtable().Reject.unwrap()(self.inner, data_ptr as *const _, data_len as u32).ok()
		}
	}

	pub fn read_limits(&self) -> Result<ReadLimits> {
		unsafe {
			let mut limits = ReadLimits {
				inbound_read_limit: 0,
				outbound_read_limit: 0,
			};
			self.vtable().GetReadLimits.unwrap()(self.inner, &mut limits.inbound_read_limit, &mut limits.outbound_read_limit)
				.ok()?;
			Ok(limits)
		}
	}

	pub fn private_data(&self) -> Result<Vec<u8>> {
		unsafe {
			let mut size = 0;
			let res = self.vtable().GetPrivateData.unwrap()(self.inner, ptr::null_mut(), &mut size);
			if res != ND_BUFFER_OVERFLOW {
				res.ok()?;
			}

			let mut data = vec![0u8; size as usize];

			if size > 0 {
				self.vtable().GetPrivateData.unwrap()(self.inner, data.as_mut_ptr() as *mut _, &mut size).ok()?;
			}

			Ok(data)
		}
	}

	pub fn local_address(&self) -> Result<SocketAddr> {
		unsafe { from_winsock_fn(self.inner, self.vtable().GetLocalAddress.unwrap()) }
	}

	pub fn peer_address(&self) -> Result<SocketAddr> {
		unsafe { from_winsock_fn(self.inner, self.vtable().GetPeerAddress.unwrap()) }
	}

	pub fn notify_disconnect(&mut self, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<()> {
		unsafe {
			let res = self.vtable().NotifyDisconnect.unwrap()(self.inner, overlapped.borrow_mut());
			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}

	pub fn disconnect(&mut self, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<()> {
		unsafe {
			let res = self.vtable().Disconnect.unwrap()(self.inner, overlapped.borrow_mut());
			if res == ND_PENDING {
				self.get_overlapped_result(overlapped, true)
			} else {
				res.ok()
			}
		}
	}
}

impl ND2Overlapped for Connector {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped {
		unsafe { &mut *(self.inner as *mut IND2Overlapped) }
	}
}

unsafe impl Send for Connector {}

impl Clone for Connector {
	fn clone(&self) -> Self {
		unsafe {
			let _n = (*(*self.inner).lpVtbl).AddRef.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Connector->AddRef()");
		}

		Self { inner: self.inner }
	}
}

impl Drop for Connector {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2Connector->Release()");
		}
	}
}
