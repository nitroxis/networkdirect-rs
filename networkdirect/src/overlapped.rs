use std::{
	borrow::{Borrow, BorrowMut},
	ptr,
};

use networkdirect_sys::IND2Overlapped;
use windows::{
	core::{Result, PCSTR},
	Win32::{
		Foundation::CloseHandle,
		System::{Threading::CreateEventA, IO::OVERLAPPED},
	},
};

pub struct Overlapped {
	pub(crate) inner: OVERLAPPED,
}

impl Overlapped {
	pub fn new() -> Result<Self> {
		let inner = OVERLAPPED {
			hEvent: unsafe { CreateEventA(None, false, false, PCSTR(ptr::null()))? },
			..Default::default()
		};

		Ok(Self { inner })
	}
}

impl Borrow<OVERLAPPED> for &mut Overlapped {
	fn borrow(&self) -> &OVERLAPPED {
		&self.inner
	}
}

impl BorrowMut<OVERLAPPED> for &mut Overlapped {
	fn borrow_mut(&mut self) -> &mut OVERLAPPED {
		&mut self.inner
	}
}

impl Drop for Overlapped {
	fn drop(&mut self) {
		unsafe {
			CloseHandle(self.inner.hEvent).unwrap();
		}
	}
}

pub trait ND2Overlapped {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped;

	fn cancel_overlapped_requests(&mut self) -> Result<()> {
		let this = self.as_overlapped_mut();
		unsafe { (*this.lpVtbl).CancelOverlappedRequests.unwrap()(this).ok() }
	}

	fn get_overlapped_result(&mut self, mut overlapped: impl BorrowMut<OVERLAPPED>, wait: bool) -> Result<()> {
		let this = self.as_overlapped_mut();
		unsafe { (*this.lpVtbl).GetOverlappedResult.unwrap()(this, overlapped.borrow_mut(), wait.into()).ok() }
	}
}
