use std::{
	ffi::{c_void, CString},
	mem,
	net::{IpAddr, SocketAddr},
	ptr,
	string::FromUtf16Error,
	sync::{Arc, Mutex},
};

use lazy_static::lazy_static;
use networkdirect_sys::*;
use thiserror::Error;
use windows::{
	core::{GUID, HRESULT, PCSTR, PCWSTR, PWSTR},
	Win32::{
		Foundation::{FreeLibrary, HMODULE, MAX_PATH},
		Networking::WinSock::{
			closesocket, WSACleanup, WSAGetLastError, WSAIoctl, WSASocketW, WSAStartup, WSCEnumProtocols, WSCGetProviderPath,
			AF_INET, AF_INET6, INVALID_SOCKET, SIO_ROUTING_INTERFACE_QUERY, SOCKADDR_STORAGE, SOCKET, SOCKET_ERROR,
			SOCK_STREAM, WSADATA, WSAENOBUFS, WSAPROTOCOL_INFOW, WSA_FLAG_OVERLAPPED, XP1_CONNECT_DATA,
			XP1_GUARANTEED_DELIVERY, XP1_GUARANTEED_ORDER, XP1_MESSAGE_ORIENTED,
		},
		System::{
			Environment::ExpandEnvironmentStringsW,
			LibraryLoader::{GetProcAddress, LoadLibraryExW, LOAD_LIBRARY_FLAGS},
		},
	},
};

use crate::{
	util::{from_winsock, to_winsock},
	Adapter, Provider,
};

const ND_SERVICE_FLAGS1: u32 = XP1_GUARANTEED_DELIVERY | XP1_GUARANTEED_ORDER | XP1_MESSAGE_ORIENTED | XP1_CONNECT_DATA;

#[derive(Error, Debug)]
pub enum ProviderError {
	#[error("WSA error")]
	Wsa(i32),

	#[error("Win32 error: {0}")]
	Win32(#[from] windows::core::Error),
}

type DllGetClassObject = unsafe extern "C" fn(rclsid: *const GUID, rrid: *const GUID, ppv: *mut *mut c_void) -> HRESULT;
type DllCanUnloadNow = unsafe extern "C" fn() -> bool;

struct ProviderDll {
	instance: HMODULE,
	get_class_object: DllGetClassObject,
	can_unload_now: DllCanUnloadNow,
}

impl ProviderDll {
	fn new(path: &[u16]) -> windows::core::Result<Self> {
		unsafe {
			#[cfg(feature = "tracing")]
			tracing::debug!(path = String::from_utf16_lossy(&path), "Loading provider DLL");

			let dll = LoadLibraryExW(PCWSTR(path.as_ptr()), None, LOAD_LIBRARY_FLAGS(0))?;

			let name1 = CString::new("DllGetClassObject").unwrap();
			let name2 = CString::new("DllCanUnloadNow").unwrap();

			let get_class_object = GetProcAddress(dll, PCSTR(name1.as_ptr() as *const u8));
			let can_unload_now = GetProcAddress(dll, PCSTR(name2.as_ptr() as *const u8));

			if get_class_object.is_none() || can_unload_now.is_none() {
				FreeLibrary(dll)?;
				return Err(windows::core::Error::from_win32());
			}

			#[cfg(feature = "tracing")]
			tracing::debug!(?dll, "Provider DLL loaded");

			Ok(ProviderDll {
				instance: dll,
				get_class_object: mem::transmute(get_class_object.unwrap()),
				can_unload_now: mem::transmute(can_unload_now.unwrap()),
			})
		}
	}
}

struct ProviderData {
	id: GUID,
	path: Vec<u16>,
	dll: Mutex<Option<ProviderDll>>,
}

#[derive(Clone)]
pub struct ProviderFactory(Arc<ProviderData>);

impl Drop for ProviderDll {
	fn drop(&mut self) {
		unsafe {
			#[cfg(feature = "tracing")]
			tracing::debug!(dll = ?self.instance, "Unloading provider DLL");

			FreeLibrary(self.instance).unwrap();
		}
	}
}

impl ProviderFactory {
	pub fn new(id: GUID) -> Result<Self, ProviderError> {
		unsafe {
			let mut path = vec![0u16; MAX_PATH as usize];
			let mut path_len = path.len() as i32;

			let mut err = 0;
			let ret = WSCGetProviderPath(&id as *const _, PWSTR(path.as_mut_ptr()), &mut path_len, &mut err);
			if ret != 0 {
				return Err(ProviderError::Wsa(err));
			}

			let len = ExpandEnvironmentStringsW(PCWSTR(path.as_ptr()), None);
			if len == 0 {
				return Err(ProviderError::Win32(windows::core::Error::from_win32()));
			}
			let mut expanded_path = vec![0u16; len as usize];

			let len = ExpandEnvironmentStringsW(PCWSTR(path.as_ptr()), Some(&mut expanded_path));
			if len == 0 {
				return Err(ProviderError::Win32(windows::core::Error::from_win32()));
			}

			#[cfg(feature = "tracing")]
			tracing::debug!(?id, path = String::from_utf16_lossy(&expanded_path), "New provider");

			Ok(Self(Arc::new(ProviderData {
				id,
				path: expanded_path,
				dll: Mutex::new(None),
			})))
		}
	}

	pub fn id(&self) -> &GUID {
		&self.0.id
	}

	pub fn path(&self) -> Result<String, FromUtf16Error> {
		let mut buf = self.0.path.as_slice();

		// Strip \0 from end
		if !buf.is_empty() && buf[buf.len() - 1] == 0 {
			buf = &buf[..(buf.len() - 1)];
		}

		String::from_utf16(buf)
	}

	pub fn load_dll(&mut self) -> windows::core::Result<()> {
		let mut dll = self.0.dll.lock().unwrap();

		if dll.is_some() {
			return Ok(());
		}

		*dll = Some(ProviderDll::new(&self.0.path)?);

		Ok(())
	}

	pub fn get_provider(&mut self) -> windows::core::Result<Provider> {
		let mut dll = self.0.dll.lock().unwrap();

		if dll.is_none() {
			*dll = Some(ProviderDll::new(&self.0.path)?);
		}

		let dll = dll.as_ref().unwrap();
		unsafe {
			let mut provider = ptr::null_mut();
			(dll.get_class_object)(&self.0.id, &IID_IND2Provider, &mut provider).ok()?;
			Ok(Provider::from_ptr(provider as *mut _))
		}
	}

	pub fn try_unload_dll(&mut self) -> bool {
		let mut lock = self.0.dll.lock().unwrap();

		if let Some(dll) = lock.as_ref() {
			unsafe {
				if !(dll.can_unload_now)() {
					return false;
				}
			}

			*lock = None;
			true
		} else {
			true
		}
	}

	fn all() -> Result<Vec<ProviderFactory>, ProviderError> {
		let mut providers = Vec::new();

		unsafe {
			let mut buffer_len = 0;
			let mut err = 0;
			let ret = WSCEnumProtocols(None, None, &mut buffer_len, &mut err);
			if ret != SOCKET_ERROR || err != WSAENOBUFS.0 {
				return Err(ProviderError::Wsa(err));
			}

			let mut buffer = vec![0u8; buffer_len as usize];

			let ret = WSCEnumProtocols(None, Some(buffer.as_mut_ptr() as *mut _), &mut buffer_len, &mut err);
			if ret < 0 {
				return Err(ProviderError::Wsa(err));
			}

			let protocols: *const WSAPROTOCOL_INFOW = buffer.as_ptr() as *const _ as *const _;

			for i in 0..(ret as usize) {
				let protocol = &(*protocols.add(i));

				if (protocol.dwServiceFlags1 & ND_SERVICE_FLAGS1) != ND_SERVICE_FLAGS1 {
					continue;
				}

				if protocol.iVersion != ND_VERSION_2 as _ {
					continue;
				}

				if protocol.iAddressFamily != AF_INET.0 as _ && protocol.iAddressFamily != AF_INET6.0 as _ {
					continue;
				}

				if protocol.iSocketType != -1 {
					continue;
				}

				if protocol.iProtocol != 0 || protocol.iProtocolMaxOffset != 0 {
					continue;
				}

				providers.push(ProviderFactory::new(protocol.ProviderId)?);
			}
		}

		Ok(providers)
	}
}

struct Framework {
	providers: Vec<ProviderFactory>,
	socket: SOCKET,
}

impl Framework {
	fn new() -> Result<Self, ProviderError> {
		let providers = ProviderFactory::all()?;
		unsafe {
			let mut wsa_data = WSADATA::default();
			let ret = WSAStartup(0x0202, &mut wsa_data);
			if ret != 0 {
				return Err(ProviderError::Wsa(ret));
			}

			let socket = WSASocketW(AF_INET.0 as i32, SOCK_STREAM.0, 0, None, 0, WSA_FLAG_OVERLAPPED);
			if socket == INVALID_SOCKET {
				WSACleanup();
				return Err(ProviderError::Wsa(WSAGetLastError().0));
			}

			Ok(Self { providers, socket })
		}
	}

	fn resolve_address(&mut self, remote_addr: SocketAddr) -> Result<Option<SocketAddr>, ProviderError> {
		let local_addr = unsafe {
			let (remote_addr, remote_addr_len) = to_winsock(remote_addr);
			let mut local_addr = SOCKADDR_STORAGE::default();
			let local_addr_len = mem::size_of::<SOCKADDR_STORAGE>() as u32;
			let mut out_len = 0;

			let ret = WSAIoctl(
				self.socket,
				SIO_ROUTING_INTERFACE_QUERY,
				Some(&remote_addr as *const _ as *const _),
				remote_addr_len,
				Some(&mut local_addr as *mut _ as *mut _),
				local_addr_len,
				&mut out_len,
				None,
				None,
			);

			if ret < 0 {
				return Err(ProviderError::Wsa(WSAGetLastError().0));
			}

			from_winsock(mem::transmute(&local_addr)).unwrap()
		};

		for provider in self.providers.iter_mut() {
			let provider = provider.get_provider()?;
			for ip in provider.query_address_list()? {
				if ip == local_addr.ip() {
					return Ok(Some(local_addr));
				}
			}
		}

		Ok(None)
	}

	fn open_adapter(&mut self, local_addr: IpAddr) -> Result<Option<Adapter>, ProviderError> {
		for provider in self.providers.iter_mut() {
			let provider = provider.get_provider()?;
			for ip in provider.query_address_list()? {
				if ip == local_addr {
					let id = provider.resolve_address(SocketAddr::new(ip, 0))?;
					return Ok(Some(provider.open_adapter(id)?));
				}
			}
		}

		Ok(None)
	}
}

impl Drop for Framework {
	fn drop(&mut self) {
		unsafe {
			closesocket(self.socket);
			WSACleanup();
		}
	}
}

lazy_static! {
	static ref FRAMEWORK: Mutex<Option<Framework>> = Mutex::new(None);
}

pub fn all_providers() -> Result<Vec<ProviderFactory>, ProviderError> {
	let mut framework = FRAMEWORK.lock().unwrap();

	if framework.is_none() {
		*framework = Some(Framework::new()?);
	}

	Ok(framework.as_ref().unwrap().providers.clone())
}

pub fn open_adapter(local_addr: impl Into<IpAddr>) -> Result<Option<Adapter>, ProviderError> {
	let mut framework = FRAMEWORK.lock().unwrap();

	if framework.is_none() {
		*framework = Some(Framework::new()?);
	}

	framework.as_mut().unwrap().open_adapter(local_addr.into())
}

pub fn resolve_address(remote_addr: impl Into<IpAddr>) -> Result<Option<IpAddr>, ProviderError> {
	let mut framework = FRAMEWORK.lock().unwrap();

	if framework.is_none() {
		*framework = Some(Framework::new()?);
	}

	Ok(framework
		.as_mut()
		.unwrap()
		.resolve_address(SocketAddr::new(remote_addr.into(), 0))?
		.map(|x| x.ip()))
}

#[cfg(test)]
mod tests {

	use crate::{all_providers, resolve_address};

	#[test]
	fn enum_providers() {
		let providers = all_providers().unwrap();

		for provider in providers {
			println!("{:?}: {}", provider.id(), provider.path().unwrap());
		}
	}

	#[test]
	fn list_addresses() {
		let providers = all_providers().unwrap();

		for mut provider in providers {
			let provider = provider.get_provider().unwrap();
			for addr in provider.query_address_list().unwrap() {
				println!("{addr}")
			}
		}
	}

	#[test]
	fn resolve() {
		let providers = all_providers().unwrap();

		for mut provider in providers {
			let provider = provider.get_provider().unwrap();
			for addr in provider.query_address_list().unwrap() {
				if addr.is_ipv4() {
					println!("{addr}");
					let addr2 = resolve_address(addr).unwrap();
					assert!(addr2.unwrap() == addr);
				}
			}
		}
	}
}
