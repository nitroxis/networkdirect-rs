use std::{borrow::BorrowMut, mem};

use bitflags::bitflags;
use networkdirect_sys::*;
use windows::core::Result;
use windows::Win32::System::IO::OVERLAPPED;

use crate::ND2Overlapped;

pub struct RemoteToken(pub u32);

pub struct LocalToken(pub u32);

bitflags! {
	pub struct RegisterFlags: u32 {
		const ALLOW_LOCAL_WRITE = ND_MR_FLAG_ALLOW_LOCAL_WRITE;
		const ALLOW_REMOTE_READ = ND_MR_FLAG_ALLOW_REMOTE_READ;
		const ALLOW_REMOTE_WRITE = ND_MR_FLAG_ALLOW_REMOTE_WRITE;
		const RDMA_READ_SINK = ND_MR_FLAG_RDMA_READ_SINK;
		const DO_NOT_SECURE_VM = ND_MR_FLAG_DO_NOT_SECURE_VM;
	}
}

pub struct UnregisteredMemoryRegion {
	pub(crate) inner: *mut IND2MemoryRegion,
}

impl AsRef<IND2MemoryRegion> for UnregisteredMemoryRegion {
	fn as_ref(&self) -> &IND2MemoryRegion {
		unsafe { &*self.inner }
	}
}

impl AsMut<IND2MemoryRegion> for UnregisteredMemoryRegion {
	fn as_mut(&mut self) -> &mut IND2MemoryRegion {
		unsafe { &mut *self.inner }
	}
}

impl UnregisteredMemoryRegion {
	pub fn vtable(&self) -> &IND2MemoryRegionVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	/// # Safety
	/// The provided pointer must be a valid IND2MemoryRegion instance. This method does not increment the reference count.
	pub unsafe fn from_ptr(ptr: *mut IND2MemoryRegion) -> UnregisteredMemoryRegion {
		Self { inner: ptr }
	}

	pub fn local_token(&self) -> LocalToken {
		LocalToken(unsafe { self.vtable().GetLocalToken.unwrap()(self.inner) })
	}

	pub fn remote_token(&self) -> RemoteToken {
		RemoteToken(unsafe { self.vtable().GetRemoteToken.unwrap()(self.inner) })
	}

	pub fn register<T, U>(
		mut self,
		buffer: T,
		flags: RegisterFlags,
		mut overlapped: impl BorrowMut<OVERLAPPED>,
	) -> Result<MemoryRegion<T>>
	where
		T: AsRef<[U]>,
	{
		let slice = buffer.as_ref();

		unsafe {
			let res = self.vtable().Register.unwrap()(
				self.inner,
				slice.as_ptr() as _,
				slice.len() as u64,
				flags.bits(),
				overlapped.borrow_mut(),
			);

			if res == ND_PENDING {
				self.get_overlapped_result(overlapped.borrow_mut(), true)?;
			} else {
				res.ok()?;
			}
		}

		let reg_mr = MemoryRegion {
			inner: self.inner,
			buffer: Some(buffer),
		};

		// Don't run `Release`.
		mem::forget(self);

		Ok(reg_mr)
	}
}

impl ND2Overlapped for UnregisteredMemoryRegion {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped {
		unsafe { &mut *(self.inner as *mut IND2Overlapped) }
	}
}

unsafe impl Send for UnregisteredMemoryRegion {}
unsafe impl Sync for UnregisteredMemoryRegion {}

impl Drop for UnregisteredMemoryRegion {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2MemoryRegion->Release()");
		}
	}
}

pub struct MemoryRegion<T> {
	pub(crate) inner: *mut IND2MemoryRegion,
	pub(crate) buffer: Option<T>,
}

impl<T> AsRef<IND2MemoryRegion> for MemoryRegion<T> {
	fn as_ref(&self) -> &IND2MemoryRegion {
		unsafe { &*self.inner }
	}
}

impl<T> AsMut<IND2MemoryRegion> for MemoryRegion<T> {
	fn as_mut(&mut self) -> &mut IND2MemoryRegion {
		unsafe { &mut *self.inner }
	}
}

impl<T> MemoryRegion<T> {
	pub fn vtable(&self) -> &IND2MemoryRegionVtbl {
		unsafe { &(*(*self.inner).lpVtbl) }
	}

	pub fn local_token(&self) -> LocalToken {
		LocalToken(unsafe { self.vtable().GetLocalToken.unwrap()(self.inner) })
	}

	pub fn remote_token(&self) -> RemoteToken {
		RemoteToken(unsafe { self.vtable().GetRemoteToken.unwrap()(self.inner) })
	}

	pub fn buffer(&self) -> &T {
		self.buffer.as_ref().unwrap()
	}

	pub fn buffer_mut(&mut self) -> &mut T {
		self.buffer.as_mut().unwrap()
	}

	pub fn deregister(mut self, mut overlapped: impl BorrowMut<OVERLAPPED>) -> Result<(UnregisteredMemoryRegion, T)> {
		unsafe {
			let res = self.vtable().Deregister.unwrap()(self.inner, overlapped.borrow_mut());

			if res == ND_PENDING {
				self.get_overlapped_result(overlapped.borrow_mut(), true)?;
			} else {
				res.ok()?;
			}
		}

		let parts = (UnregisteredMemoryRegion { inner: self.inner }, self.buffer.take().unwrap());

		// Don't run `Release`.
		mem::forget(self);

		Ok(parts)
	}
}

impl<T> ND2Overlapped for MemoryRegion<T> {
	fn as_overlapped_mut(&mut self) -> &mut IND2Overlapped {
		unsafe { &mut *(self.inner as *mut IND2Overlapped) }
	}
}

unsafe impl<T: Send> Send for MemoryRegion<T> {}
unsafe impl<T: Sync> Sync for MemoryRegion<T> {}

impl<T> Drop for MemoryRegion<T> {
	fn drop(&mut self) {
		unsafe {
			let _n = self.vtable().Release.unwrap()(self.inner);
			#[cfg(feature = "tracing")]
			tracing::trace!(n = _n, "IND2MemoryRegion->Release()");
		}
	}
}
