use std::{
	mem,
	net::{Ipv4Addr, Ipv6Addr, SocketAddr, SocketAddrV4, SocketAddrV6},
	ptr,
};

use networkdirect_sys::ND_BUFFER_OVERFLOW;
use windows::{
	core::{Result, HRESULT},
	Win32::{
		Foundation::{E_NOTIMPL, STATUS_BUFFER_TOO_SMALL},
		Networking::WinSock::{AF_INET, AF_INET6, SOCKADDR, SOCKADDR_IN, SOCKADDR_IN6, SOCKADDR_INET},
	},
};

pub fn from_winsock(addr: &SOCKADDR) -> Option<SocketAddr> {
	unsafe {
		if addr.sa_family == AF_INET {
			let addr: &SOCKADDR_IN = mem::transmute(addr);
			let ip = Ipv4Addr::from(u32::from_be(addr.sin_addr.S_un.S_addr));
			let port = u16::from_be(addr.sin_port);
			Some(SocketAddr::V4(SocketAddrV4::new(ip, port)))
		} else if addr.sa_family == AF_INET6 {
			let addr: &SOCKADDR_IN6 = mem::transmute(addr);
			let ip = Ipv6Addr::from(addr.sin6_addr.u.Byte);
			let port = u16::from_be(addr.sin6_port);
			Some(SocketAddr::V6(SocketAddrV6::new(
				ip,
				port,
				addr.sin6_flowinfo,
				addr.Anonymous.sin6_scope_id,
			)))
		} else {
			None
		}
	}
}

pub type GetAddressFn<T> = unsafe extern "C" fn(*mut T, *mut SOCKADDR, *mut u32) -> HRESULT;

pub unsafe fn from_winsock_fn<T>(this: *mut T, f: GetAddressFn<T>) -> Result<SocketAddr> {
	let mut size = 0;
	let res = f(this, ptr::null_mut(), &mut size);
	if res != ND_BUFFER_OVERFLOW && res.0 != STATUS_BUFFER_TOO_SMALL.0 {
		res.ok()?;
	}

	let mut data = vec![0u8; size as usize];
	f(this, data.as_mut_ptr() as *mut _, &mut size).ok()?;

	let addr: &SOCKADDR = &*(data.as_ptr() as *const _);
	from_winsock(addr).ok_or_else(|| E_NOTIMPL.into())
}

pub fn to_winsock(addr: SocketAddr) -> (SOCKADDR_INET, u32) {
	unsafe {
		let addr: SOCKADDR_INET = addr.into();
		let addr_len = if addr.si_family == AF_INET {
			mem::size_of::<SOCKADDR_IN>()
		} else if addr.si_family == AF_INET6 {
			mem::size_of::<SOCKADDR_IN6>()
		} else {
			0
		};
		(addr, addr_len as u32)
	}
}
