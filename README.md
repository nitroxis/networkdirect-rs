NetworkDirect
=============

This crate attempts to provide a safe wrapper around Microsoft's NetworkDirect SPI, version 2.0.

>The NetworkDirect architecture provides application developers with a networking interface that enables zero-copy data transfers between applications, kernel-bypass I/O generation and completion processing, and one-sided data transfer operations. The NetworkDirect service provider interface (SPI) defines the interface that NetworkDirect providers implement to expose their hardware capabilities to applications.

In addition to wrapping the NetworkDirect interfaces, it also implements the ND support library (`ndutil.lib` / `ndsupport.h`) in pure Rust via the `windows` crate. This allows easy initialization and enumeration of available NetworkDirect-capable hardware.

Documentation
-------------

Documentation for NetworkDirect APIs can be found [in Microsoft's repository](https://github.com/microsoft/NetworkDirect/blob/master/docs/NetworkDirectSPI.md).

License
-------

MIT